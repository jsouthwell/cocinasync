unit cocinasync.flux.action;

interface

uses System.SysUtils, System.Classes, System.Generics.Collections;

type
  TBaseAction = class;

  EFluxActionException = class(Exception);

  TActionClass = class of TBaseAction;

  TBaseAction = class(TObject)
  strict private
    class var FCS : TMREWSync;
    class var FActionPool : TDictionary<TClass, TQueue<TBaseAction>>;
  strict private
    FSession : string;
  protected
    {$If CompilerVersion > 30}
    class function New<AC : TBaseAction, constructor> : AC; overload;
    class function New<AC : TBaseAction, constructor>(Session : string) : AC; overload;
    class function New(ClassType : TActionClass) : TBaseAction; overload;
    {$ELSE}
    class function New(ClassType : TActionClass) : TBaseAction;
    {$IFEND}
    class procedure Finish(Action : TBaseAction); overload;
    class procedure ClearPools;
  public
    property Session : string read FSession;
    constructor Create; virtual;

    class constructor Create;
    class destructor Destroy;

    procedure Finish; overload;
    function AccumulateActions : boolean; virtual;
    procedure Dispatch; reintroduce; overload;
  end;

  TActionEvent<AC : TBaseAction> = procedure(Sender : TObject; Action : AC) of object;


implementation

uses
  Cocinasync.Flux;

{ TBaseAction }

function TBaseAction.AccumulateActions: boolean;
begin
  Result := True;
end;

class procedure TBaseAction.ClearPools;
var
  ary : TArray<TPair<TClass, TQueue<TBaseAction>>>;
  p: TPair<TClass, TQueue<TBaseAction>>;
begin
  FCS.BeginWrite;
  try
    ary := FActionPool.ToArray;
    for p in ary do
    begin
      TMonitor.Enter(p.Value);
      try
        while p.Value.Count > 0 do
          p.Value.Dequeue.Free;
      finally
        TMonitor.Exit(p.Value);
      end;
    end;
  finally
    FCS.EndWrite;
  end;
end;

class constructor TBaseAction.Create;
begin
  FCS := TMREWSync.Create;
  FActionPool := TDictionary<TClass, TQueue<TBaseAction>>.Create;
end;

constructor TBaseAction.Create;
begin
  inherited Create;
end;

class destructor TBaseAction.Destroy;
var
  ary : TArray<TPair<TClass, TQueue<TBaseAction>>>;
  p: TPair<TClass, TQueue<TBaseAction>>;
begin
  FCS.BeginWrite;
  try
    ary := FActionPool.ToArray;
    for p in ary do
    begin
      TMonitor.Enter(p.Value);
      try
        while p.Value.Count > 0 do
          p.Value.Dequeue.Free;
      finally
        TMonitor.Exit(p.Value);
      end;
      p.Value.Free;
    end;
  finally
    FCS.EndWrite;
  end;
  FCS.Free;
  FActionPool.Free;
end;

procedure TBaseAction.Dispatch;
begin
  Flux.Dispatch(Self);
end;

procedure TBaseAction.Finish;
begin
  Finish(Self);
end;

class function TBaseAction.New(ClassType: TActionClass): TBaseAction;
var
  queue : TQueue<TBaseAction>;
begin
  Result := nil;
  TBaseAction.FCS.BeginRead;
  try
    if TBaseAction.FActionPool.ContainsKey( ClassType ) then
    begin
      queue := TBaseAction.FActionPool[ClassType];
      TMonitor.Enter(queue);
      try
        if queue.Count > 0 then
          Result := queue.Dequeue
      finally
        TMonitor.Exit(queue);
      end;
    end;
  finally
    TBaseAction.FCS.EndRead;
  end;

  if Result = nil then
    Result := ClassType.Create;
end;

{$If CompilerVersion > 30}

class function TBaseAction.New<AC>(Session: string): AC;
var
  queue : TQueue<TBaseAction>;
begin
  Result := nil;

  if Session='*' then
    raise EFluxActionException.Create('Wildcard session actions not supported.');

  FCS.BeginRead;
  try
    if FActionPool.ContainsKey(AC) then
    begin
      queue := FActionPool[AC];
      TMonitor.Enter(queue);
      try
        if queue.Count > 0 then
          Result := AC(queue.Dequeue)
      finally
        TMonitor.Exit(queue);
      end;
    end;
  finally
    FCS.EndRead;
  end;

  if Result = nil then
    Result := AC.Create;

  Result.FSession := Session;
end;

class function TBaseAction.New<AC>: AC;
begin
  Result := New<AC>('');
end;
{$IFEND}

class procedure TBaseAction.Finish(Action: TBaseAction);
var
  queue : TQueue<TBaseAction>;
begin
  FCS.BeginRead;
  try
    if FActionPool.ContainsKey(Action.ClassType) then
    begin
      queue := FActionPool[Action.ClassType];
      TMonitor.Enter(queue);
      try
        queue.Enqueue(Action);
      finally
        TMonitor.Exit(queue);
      end;
    end else
    begin
      FCS.BeginWrite;
      try
        if FActionPool.ContainsKey(Action.ClassType) then
        begin
          Finish(Action);
          exit;
        end;
        queue := TQueue<TBaseAction>.Create;
        queue.Enqueue(Action);
        FActionPool.Add(Action.ClassType, queue);
      finally
        FCS.EndWrite;
      end;
    end;
  finally
    FCS.EndRead;
  end;

end;

end.
