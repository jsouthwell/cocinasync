unit cocinasync.flux.view.vcl;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  VCL.Forms,
  VCL.Controls,
  cocinasync.flux.action,
  cocinasync.flux.store,
  cocinasync.flux;

type
  IOnAction<T : TBaseAction> = interface;

  IWithForm<T : TBaseAction> = interface(IInterface)
    procedure ThenFree;
    procedure ThenNothing;
  end;

  TFormHandler = reference to procedure(Form : TCustomForm);
  TFrameHandler = reference to procedure(Frame : TCustomFrame);

  IOnAction<T : TBaseAction> = interface(IInterface)
    function Create(FormClass : TCustomFormClass) : IWithForm<T>; overload;
    function Create(FrameClass : TCustomFrameClass; ToParent : TWinControl) : IWithForm<T>; overload;
    function Create(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm>) : IWithForm<T>; overload;
    function Create(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame>) : IWithForm<T>; overload;
    function Create(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm, T>) : IWithForm<T>; overload;
    function Create(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame, T>) : IWithForm<T>; overload;

    function CreateAndShow(FormClass : TCustomFormClass) : IWithForm<T>; overload;
    function CreateAndShow(FrameClass : TCustomFrameClass; ToParent : TWinControl) : IWithForm<T>; overload;
    function CreateAndShow(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm>) : IWithForm<T>; overload;
    function CreateAndShow(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame>) : IWithForm<T>; overload;
    function CreateAndShow(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm, T>) : IWithForm<T>; overload;
    function CreateAndShow(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame, T>) : IWithForm<T>; overload;

    function &Do(Handler : TProc) : IWithForm<T>; overload;
    function &Do(Handler : TFormHandler) : IWithForm<T>; overload;
    function &Do(Handler : TFrameHandler) : IWithForm<T>; overload;

    function &With(Form : TCustomForm) : IWithForm<T>; overload;
    function &With(Frame : TCustomFrame) : IWithForm<T>; overload;

    function Show(Form : TCustomForm) : IWithForm<T>; overload;
    function Show(Frame : TCustomFrame) : IWithForm<T>; overload;
    function Show : IWithForm<T>; overload;
    function Show(Form : TCustomForm; OnShow : TProc<T>) : IWithForm<T>; overload;
    function Show(Frame : TCustomFrame; OnShow : TProc<T>) : IWithForm<T>; overload;
    function Show(OnShow : TProc<TCustomForm, T>) : IWithForm<T>; overload;
    function Show(OnShow : TProc<TCustomFrame, T>) : IWithForm<T>; overload;

    function Hide(Form : TCustomForm) : IWithForm<T>; overload;
    function Hide(Frame : TCustomFrame) : IWithForm<T>; overload;
    function Hide : IWithForm<T>; overload;
    function Hide(Form : TCustomForm; OnHide : TProc<T>) : IWithForm<T>; overload;
    function Hide(Frame : TCustomFrame; OnHide : TProc<T>) : IWithForm<T>; overload;
    function Hide(OnHide : TProc<TCustomForm, T>) : IWithForm<T>; overload;
    function Hide(OnHide : TProc<TCustomFrame, T>) : IWithForm<T>; overload;

    function Maximize : IWithForm<T>; overload;
    function Maximize(OnMaximize : TProc<T>) : IWithForm<T>; overload;

    function Minimize : IWithForm<T>; overload;
    function Minimize(OnMinimize : TProc<T>) : IWithForm<T>; overload;

  end;

  TViews = class
  strict private
    type
      // this class has no business being in the interface section. It's entirely private yet Delphi will not permit it, so it's going in as a strict private nested class
      TViewsImpl<T : TBaseAction> = class(TBaseStore, IOnAction<T>, IWithForm<T>)
      private
        FContextForm : TCustomForm;
        FContextFrame : TCustomFrame;
        FToDo : TList<TFunc<Boolean>>;
        FCurrentAction : T;

        procedure ThenFree;
        procedure ThenNothing;

        function Create(FormClass : TCustomFormClass) : IWithForm<T>; reintroduce; overload;
        function Create(FrameClass : TCustomFrameClass; ToParent : TWinControl) : IWithForm<T>; reintroduce; overload;
        function Create(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm>) : IWithForm<T>; reintroduce; overload;
        function Create(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame>) : IWithForm<T>; reintroduce; overload;
        function Create(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm, T>) : IWithForm<T>; reintroduce; overload;
        function Create(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame, T>) : IWithForm<T>; reintroduce; overload;

        function CreateAndShow(FormClass : TCustomFormClass) : IWithForm<T>; overload;
        function CreateAndShow(FrameClass : TCustomFrameClass; ToParent : TWinControl) : IWithForm<T>; overload;
        function CreateAndShow(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm>) : IWithForm<T>; overload;
        function CreateAndShow(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame>) : IWithForm<T>; overload;
        function CreateAndShow(FormClass : TCustomFormClass; OnCreate : TProc<TCustomForm, T>) : IWithForm<T>; overload;
        function CreateAndShow(FrameClass : TCustomFrameClass; ToParent : TWinControl; OnCreate : TProc<TCustomFrame, T>) : IWithForm<T>; overload;

        function &Do(Handler : TProc) : IWithForm<T>; overload;
        function &Do(Handler : TFormHandler) : IWithForm<T>; overload;
        function &Do(Handler : TFrameHandler) : IWithForm<T>; overload;

        function &With(Form : TCustomForm) : IWithForm<T>; overload;
        function &With(Frame : TCustomFrame) : IWithForm<T>; overload;

        function Show(Form : TCustomForm) : IWithForm<T>; overload;
        function Show(Frame : TCustomFrame) : IWithForm<T>; overload;
        function Show : IWithForm<T>; overload;
        function Show(Form : TCustomForm; OnShow : TProc<T>) : IWithForm<T>; overload;
        function Show(Frame : TCustomFrame; OnShow : TProc<T>) : IWithForm<T>; overload;
        function Show(OnShow : TProc<TCustomForm, T>) : IWithForm<T>; overload;
        function Show(OnShow : TProc<TCustomFrame, T>) : IWithForm<T>; overload;

        function Hide(Form : TCustomForm) : IWithForm<T>; overload;
        function Hide(Frame : TCustomFrame) : IWithForm<T>; overload;
        function Hide : IWithForm<T>; overload;
        function Hide(Form : TCustomForm; OnHide : TProc<T>) : IWithForm<T>; overload;
        function Hide(Frame : TCustomFrame; OnHide : TProc<T>) : IWithForm<T>; overload;
        function Hide(OnHide : TProc<TCustomForm, T>) : IWithForm<T>; overload;
        function Hide(OnHide : TProc<TCustomFrame, T>) : IWithForm<T>; overload;

        function Maximize : IWithForm<T>; overload;
        function Maximize(OnMaximize : TProc<T>) : IWithForm<T>; overload;

        function Minimize : IWithForm<T>; overload;
        function Minimize(OnMinimize : TProc<T>) : IWithForm<T>; overload;
      public
        constructor DoCreate; overload;
        destructor Destroy; override;
      end;
  strict private
    class var FViews : TList<TBaseStore>;
  public
    class function &On<T : TBaseAction> : IOnAction<T>;
    class constructor Create;
    class destructor Destroy;
  end;

implementation

uses
  cocinasync.async;

{ TViews }

class constructor TViews.Create;
begin
  FViews := TList<TBaseStore>.Create;
end;

type
  TIntfObjHack = class(TInterfacedObject)
  end;

class destructor TViews.Destroy;
var
  v: TBaseStore;
begin
  for v in FViews do
  begin
    TIntfObjHack(v)._Release;
  end;
  FViews.Free;
end;

class function TViews.&On<T> : IOnAction<T>;
var
  v : TViewsImpl<T>;
begin
  v := TViewsImpl<T>.DoCreate;
  FViews.Add(v);
  v._AddRef;
  Result := v;
end;

{ TViewsImpl }

constructor TViews.TViewsImpl<T>.DoCreate;
begin
  inherited Create;
  FToDo := TList<TFunc<Boolean>>.Create;

  Flux.Register<T>(Self,
    procedure(Action : T)
    begin
      FCurrentAction := Action;
      TAsync.SynchronizeIfInThread(
        procedure
        var
          p: TFunc<Boolean>;
        begin
          for p in FToDo do
          begin
            if not p() then
              break;
          end;
        end
      );
    end

  );
end;

destructor TViews.TViewsImpl<T>.Destroy;
begin
  FToDo.Free;
  inherited;
end;

function TViews.TViewsImpl<T>.CreateAndShow(FormClass: TCustomFormClass; OnCreate: TProc<TCustomForm, T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := FormClass.Create(nil);
      OnCreate(FContextForm, FCurrentAction);
      FContextFrame.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.CreateAndShow(FormClass: TCustomFormClass): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := FormClass.Create(nil);
      FContextFrame.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.CreateAndShow(FrameClass: TCustomFrameClass; ToParent: TWinControl; OnCreate: TProc<TCustomFrame>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := FrameClass.Create(nil);
      FContextFrame.Parent := ToParent;
      OnCreate(FContextFrame);
      FContextFrame.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.CreateAndShow(FrameClass: TCustomFrameClass;
  ToParent: TWinControl): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := FrameClass.Create(nil);
      FContextFrame.Parent := ToParent;
      FContextFrame.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Create(FormClass: TCustomFormClass;
  OnCreate: TProc<TCustomForm>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := FormClass.Create(nil);
      OnCreate(FContextForm);
    end
  );
end;

function TViews.TViewsImpl<T>.Create(FrameClass: TCustomFrameClass;
  ToParent: TWinControl): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := FrameClass.Create(nil);
      FContextFrame.Parent := ToParent;
    end
  );
end;

function TViews.TViewsImpl<T>.Create(FormClass: TCustomFormClass): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := FormClass.Create(nil);
    end
  );
end;

function TViews.TViewsImpl<T>.&Do(Handler : TProc): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      Handler();
    end
  );
end;

function TViews.TViewsImpl<T>.&Do(Handler: TFrameHandler): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      Handler(FContextFrame);
    end
  );
end;

function TViews.TViewsImpl<T>.&Do(Handler: TFormHandler): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      Handler(FContextForm);
    end
  );
end;

function TViews.TViewsImpl<T>.Create(FrameClass: TCustomFrameClass;
  ToParent: TWinControl;
  OnCreate: TProc<TCustomFrame, T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := FrameClass.Create(nil);
      FContextFrame.Parent := ToParent;
      OnCreate(FContextFrame, FCurrentAction);
    end
  );
end;

function TViews.TViewsImpl<T>.Create(FormClass: TCustomFormClass;
  OnCreate: TProc<TCustomForm, T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := FormClass.Create(nil);
      OnCreate(FContextForm, FCurrentAction);
    end
  );
end;

function TViews.TViewsImpl<T>.Create(FrameClass: TCustomFrameClass;
  ToParent: TWinControl; OnCreate: TProc<TCustomFrame>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := FrameClass.Create(nil);
      FContextFrame.Parent := ToParent;
      OnCreate(FContextFrame);
    end
  );
end;

function TViews.TViewsImpl<T>.CreateAndShow(FrameClass: TCustomFrameClass;
  ToParent: TWinControl; OnCreate: TProc<TCustomFrame, T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := FrameClass.Create(nil);
      FContextFrame.Parent := ToParent;
      OnCreate(FContextFrame, FCurrentAction);
      FContextFrame.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.CreateAndShow(FormClass: TCustomFormClass;
  OnCreate: TProc<TCustomForm>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := FormClass.Create(nil);
      OnCreate(FContextForm);
      FContextForm.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Hide(Form: TCustomForm;
  OnHide: TProc<T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := Form;
      OnHide(FCurrentAction);
      FContextForm.Hide;
    end
  );
end;

function TViews.TViewsImpl<T>.Hide: IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm.Hide;
    end
  );
end;

function TViews.TViewsImpl<T>.Hide(Frame: TCustomFrame): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := Frame;
      FContextFrame.Hide;
    end
  );
end;

function TViews.TViewsImpl<T>.Hide(Frame: TCustomFrame;
  OnHide: TProc<T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := Frame;
      OnHide(FCurrentAction);
      FContextFrame.Hide;
    end
  );
end;

function TViews.TViewsImpl<T>.Hide(OnHide : TProc<TCustomForm, T>) : IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      OnHide(FContextForm, FCurrentAction);
      FContextForm.Hide;
    end
  );
end;

function TViews.TViewsImpl<T>.Hide(OnHide : TProc<TCustomFrame, T>) : IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      OnHide(FContextFrame, FCurrentAction);
      FContextFrame.Hide;
    end
  );
end;

function TViews.TViewsImpl<T>.Hide(Form: TCustomForm): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := Form;
      FContextForm.Hide;
    end
  );
end;

function TViews.TViewsImpl<T>.Maximize: IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm.WindowState := wsMaximized;
    end
  );
end;

function TViews.TViewsImpl<T>.Maximize(OnMaximize: TProc<T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      OnMaximize(FCurrentAction);
      FContextForm.WindowState := wsMaximized;
    end
  );
end;

function TViews.TViewsImpl<T>.Minimize(OnMinimize: TProc<T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      OnMinimize(FCurrentAction);
      FContextForm.WindowState := wsMinimized;
    end
  );
end;

function TViews.TViewsImpl<T>.Minimize: IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm.WindowState := wsMinimized;
    end
  );
end;

function TViews.TViewsImpl<T>.Show(Form: TCustomForm): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := Form;
      FContextForm.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Show(OnShow: TProc<TCustomForm, T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      OnShow(FContextForm, FCurrentAction);
      FContextForm.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Show(OnShow: TProc<TCustomFrame, T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      OnShow(FContextFrame, FCurrentAction);
      FContextForm.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Show(Frame: TCustomFrame;
  OnShow: TProc<T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := Frame;
      OnShow(FCurrentAction);
      FContextFrame.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Show(Form: TCustomForm;
  OnShow: TProc<T>): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := Form;
      OnShow(FCurrentAction);
      FContextForm.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Show(Frame: TCustomFrame): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := Frame;
      FContextFrame.Show;
    end
  );
end;

function TViews.TViewsImpl<T>.Show: IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      if Assigned(FContextForm) then
        FContextForm.Show
      else
        FContextFrame.Show;
    end
  );
end;

procedure TViews.TViewsImpl<T>.ThenNothing;
begin
  FToDo.Add(
    function : boolean
    begin
      Result := False;
    end
  );
end;

procedure TViews.TViewsImpl<T>.ThenFree;
begin
  FToDo.Add(
    function : boolean
    begin
      Result := False;
      if Assigned(FContextForm) then
        FreeAndNil(FContextForm);
      if Assigned(FContextFrame) then
        FreeAndNil(FContextFrame);
    end
  );
end;

function TViews.TViewsImpl<T>.&With(Form: TCustomForm): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextForm := Form;
    end
  );
end;

function TViews.TViewsImpl<T>.&With(Frame: TCustomFrame): IWithForm<T>;
begin
  Result := Self;
  FToDo.Add(
    function : Boolean
    begin
      Result := True;
      FContextFrame := Frame;
    end
  );
end;

end.
