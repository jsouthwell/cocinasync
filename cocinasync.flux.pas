unit cocinasync.flux;

interface

uses
  System.SysUtils,
  System.Classes,
  cocinasync.flux.dispatcher,
  cocinasync.flux.store;

var
  Flux : TFluxDispatcher;
  Stores : TStores;

implementation

initialization
  Flux := TFluxDispatcher.Create;
  Stores := TStores.Create;

finalization
  Flux.Free;
  Stores.Free;

end.
