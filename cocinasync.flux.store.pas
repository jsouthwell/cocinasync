unit cocinasync.flux.store;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  System.Rtti,
  System.SyncObjs;

type
  TBaseStore = class;
  TStoreID = string;

  TEventMethod<T> = procedure(Sender : TObject; Store : T) of object;

  EFluxBindException = class(Exception);

  TBaseStore = class(TInterfacedObject)
  private type
    IStoreBinding = interface
      function Target: TComponent;
      function SourceProperty : string;
      function TargetProperty : string;
      function AsValue : TValue;
      procedure UpdateValue;
    end;
  public
    const INFINITE = High(Integer);
  private
    FUpdateEvent : TEvent;
    FViews : TDictionary<TObject, TProc<TBaseStore>>;
    FBindings : TList<IStoreBinding>;
    FCS : TCriticalSection;
    FUpdateCount : UInt64;
    FSession : string;
  protected
    procedure SafeUpdate(OnUpdate : TProc); virtual;
    procedure Bind(const SourceProperty : string; Target : TComponent; const TargetProperty : string);
    procedure UnbindAll(Target : TComponent);
    procedure UpdateBindings;
  public
    constructor Create; reintroduce; virtual;
    constructor CreateWithSession(Session : string); virtual;
    destructor Destroy; override;
    function AutoViewUpdate : boolean; virtual;
    property UpdateCount : UInt64 read FUpdateCount;
    property Session : string read FSession;

    {$If CompilerVersion > 30}
    procedure RegisterForUpdates<SC : TBaseStore>(Context : TObject; Event : TEventMethod<SC>); overload;
    procedure RegisterForUpdates<SC : TBaseStore>(Context : TObject; Handler : TProc<SC>); overload;
    function WaitForUpdate<SC : TBaseStore>(Handler : TProc<SC>; Timeout : Integer = INFINITE) : boolean; overload;
    {$ELSE}
    procedure RegisterForUpdates(Context : TObject; Event : TEventMethod<TBaseStore>); overload;
    procedure RegisterForUpdates(Context : TObject; Handler : TProc<TBaseStore>); overload;
    function WaitForUpdate(Handler : TProc<TBaseStore>; Timeout : Integer = INFINITE) : boolean; overload;
    {$IFEND}
    procedure UnregisterForUpdates(Context : TObject);
    procedure UpdateViews; virtual;
  end;


  TBaseStoreClass = class of TBaseStore;

  TDataStoreHandler<SC : TBaseStore, constructor> = reference to procedure(Store : SC);

  TStores = class sealed
  strict private
    type
      TSessionStores = TDictionary<TBaseStoreClass, TBaseStore>;
      TStoreIndex = TDictionary<String, TSessionStores>;
  strict private
    class var FStores : TStoreIndex;
    class var FCS : TCriticalSection;
  private
    class function Stores(Session : string) : TSessionStores;
  public
    class procedure WithData<SC : TBaseStore, constructor>(Session : string; OnData : TDataStoreHandler<SC>); overload;
    class procedure WithData<SC : TBaseStore, constructor>(OnData : TDataStoreHandler<SC>); overload;
    class procedure WaitForUpdate<SC : TBaseStore, constructor>(Session : string; LastUpdateCount : UInt64; OnData : TDataStoreHandler<SC>; Timeout : Cardinal = TBaseStore.INFINITE);

    // Bindings currently only support single level property names.  Items instead of Items.Text to assign a string list for example.
    // If you need more complex bindings, use the RegisterForUpdates overloads
    class procedure Bind<SC : TBaseStore, constructor>(Target : TComponent; const &PropertyName : String); overload;
    class procedure Bind<SC : TBaseStore, constructor>(const SourceProperty : string; Target : TComponent; const TargetProperty : String); overload;
    class procedure Bind<SC : TBaseStore, constructor>(Target : TComponent; const &PropertyName : String; const Session : string); overload;
    class procedure Bind<SC : TBaseStore, constructor>(const SourceProperty : string; Target : TComponent; const TargetProperty : String; const Session : string); overload;
    class procedure UnbindAll<SC : TBaseStore, constructor>(Target : TComponent); overload;
    class procedure UnbindAll<SC : TBaseStore, constructor>(Target : TComponent; const Session : string); overload;

    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Sessions : TArray<string>; Context : TObject; Event : TEventMethod<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Sessions : TArray<string>; Context : TObject; Handler : TProc<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Session : string; Context : TObject; Event : TEventMethod<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Session : string; Context : TObject; Handler : TProc<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Context : TObject; Event : TEventMethod<SC>); overload;
    class procedure RegisterForUpdates<SC : TBaseStore, constructor>(Context : TObject; Handler : TProc<SC>); overload;
    class procedure UnregisterForUpdates<SC : TBaseStore>(Sessions : TArray<string>; Context : TObject); overload;
    class procedure UnregisterForUpdates<SC : TBaseStore>(Session : string; Context : TObject); overload;
    class procedure UnregisterForUpdates<SC : TBaseStore>(Context : TObject); overload;
    class constructor Create;
    class destructor Destroy;
  end;

  TBaseDataModuleStore = class(TDataModule)
  private
    FBaseStore: TBaseStore;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    {$If CompilerVersion > 30}
    procedure RegisterForUpdates<SC : TBaseDataModuleStore>(Context : TObject; Handler : TProc<SC>); overload;
    {$ENDIF}
    procedure RegisterForUpdates(Context : TObject; Handler : TProc<TBaseDataModuleStore>); {$If CompilerVersion > 30}overload;{$ENDIF}
    procedure UpdateViews; virtual;
    property BaseStore : TBaseStore read FBaseStore;
    procedure UnregisterForUpdates(Context : TObject);
  end;

implementation

uses
  cocinasync.async;

type
  TBinding = class(TInterfacedObject, TBaseStore.IStoreBinding)
  private
    FOwner : TBaseStore;
    FTarget : TComponent;
    FSourceProperty : string;
    FTargetProperty : string;
    FSourceProp : TRTTIProperty;
    FTargetProp : TRTTIProperty;
    FIsInstanceProp: Boolean;
    FSourcePropInstance: TObject;
    FTargetPropInstance: TObject;
  public
    constructor Create(Owner : TBaseStore; Target: TComponent; const SourceProperty, TargetProperty : string);
    function Target : TComponent;
    function SourceProperty : string;
    function TargetProperty : string;
    function AsValue : TValue;
    procedure UpdateValue;
  end;

{ TBaseStore }

function TBaseStore.AutoViewUpdate: boolean;
begin
  Result := True;
end;

procedure TBaseStore.Bind(const SourceProperty: string; Target: TComponent; const TargetProperty : string);
begin
  FBindings.Add(
    TBinding.Create(Self, Target, SourceProperty, TargetProperty)
  );
end;

constructor TBaseStore.Create;
begin
  inherited;
  FBindings := TList<IStoreBinding>.Create;
  FUpdateEvent := TEvent.Create;
  FCS := TCriticalSection.Create;
  FViews := TDictionary<TObject, TProc<TBaseStore>>.Create;
end;

constructor TBaseStore.CreateWithSession(Session: string);
begin
  FSession := Session;
  Create;
end;

destructor TBaseStore.Destroy;
begin
  FCS.Free;
  FViews.Free;
  FUpdateEvent.Free;
  FBindings.Free;
  inherited;
end;

{$IF CompilerVersion > 30}
procedure TBaseStore.RegisterForUpdates<SC>(Context: TObject; Event: TEventMethod<SC>);
begin
  RegisterForUpdates<SC>(
    Context,
    procedure(Store : SC)
    begin
      Event(Context, Store);
    end
  );
end;

procedure TBaseStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>);
begin
  TMonitor.Enter(FViews);
  try
    FViews.AddOrSetValue(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    );
    Handler(Self);
  finally
    TMonitor.Exit(FViews);
  end;
end;

function TBaseStore.WaitForUpdate<SC>(Handler : TProc<SC>; Timeout : Integer = INFINITE) : boolean;
begin
  if FUpdateEvent.WaitFor(Timeout) = wrSignaled then
  begin
    Handler(Self as SC);
    Result := True;
  end else
    Result := False;
end;

{$ELSE}

procedure TBaseStore.RegisterForUpdates(Context: TObject; Event: TEventMethod<TBaseStore>);
begin
  RegisterForUpdates(Context,
    procedure(Store : TBaseStore)
    begin
      Event(Context, Store);
    end
  );
end;

procedure TBaseStore.RegisterForUpdates(Context : TObject; Handler: TProc<TBaseStore>);
begin
  TMonitor.Enter(FViews);
  try
    FViews.AddOrSetValue(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    );
    Handler(Self);
  finally
    TMonitor.Exit(FViews);
  end;
end;

function TBaseStore.WaitForUpdate(Handler: TProc<TBaseStore>; Timeout : Integer = INFINITE) : boolean;
begin
  if FUpdateEvent.WaitFor(Timeout) = wrSignaled then
  begin
    Handler(Self);
    Result := True;
  end else
    Result := False;
end;
{$IFEND}

procedure TBaseStore.SafeUpdate(OnUpdate: TProc);
begin
  FCS.TryEnter;
  try
    OnUpdate();
  finally
    FCS.Leave;
  end;
end;

procedure TBaseStore.UnbindAll(Target: TComponent);
var
  i: Integer;
begin
  for i := FBindings.Count-1 downto 0 do
  begin
    if FBindings[i].Target = Target then
    {$IF CompilerVersion > 30}
      FBindings.ExtractAt(i);
    {$ELSE}
      FBindings.Extract(FBindings[i]);
    {$ENDIF}
  end;
end;

procedure TBaseStore.UnregisterForUpdates(Context: TObject);
begin
  TMonitor.Enter(FViews);
  try
    FViews.Remove(Context);
  finally
    TMonitor.Exit(FViews);
  end;
end;

procedure TBaseStore.UpdateBindings;
var
  binding : TBaseStore.IStoreBinding;
begin
  for binding in FBindings do
    binding.UpdateValue;
end;

procedure TBaseStore.UpdateViews;
var
  ary : TArray<TPair<TObject, TProc<TBaseStore>>>;
  a: TPair<TObject, TProc<TBaseStore>>;
begin
  UpdateBindings;

  TMonitor.Enter(FViews);
  try
    ary := FViews.ToArray;
  finally
    TMonitor.Exit(FViews);
  end;
  for a in ary do
  begin
    TAsync.SynchronizeIfInThread(
      procedure
      begin
        a.Value(Self);
      end
    );
  end;
  inc(FUpdateCount);
  FUpdateEvent.SetEvent;
  FUpdateEvent.ResetEvent;
end;

{ TStores }

class procedure TStores.Bind<SC>(Target: TComponent; const PropertyName: String);
begin
  Bind<SC>(PropertyName, Target, PropertyName, '');
end;

class procedure TStores.Bind<SC>(Target: TComponent; const PropertyName, Session: string);
begin
  Bind<SC>(PropertyName, Target, PropertyName, Session);
end;

class procedure TStores.Bind<SC>(const SourceProperty: string; Target: TComponent; const TargetProperty, Session: string);
begin
  WithData<SC>(Session,
    procedure(Store : SC)
    begin
      Store.Bind(SourceProperty, Target, TargetProperty);
    end
  );
end;

class procedure TStores.Bind<SC>(const SourceProperty: string; Target: TComponent; const TargetProperty: String);
begin
  Bind<SC>(SourceProperty, Target, TargetProperty, '');
end;

class constructor TStores.Create;
begin
  FCS := TCriticalSection.Create;
  FStores := TStoreIndex.Create;
end;

class destructor TStores.Destroy;
begin
  FCS.Free;
  FStores.Free;
end;

class procedure TStores.RegisterForUpdates<SC>(Context: TObject; Handler: TProc<SC>);
begin
  RegisterForUpdates<SC>([''], Context, Handler);
end;

class procedure TStores.RegisterForUpdates<SC>(Sessions: TArray<string>;
  Context: TObject; Event: TEventMethod<SC>);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if not st.ContainsKey(SC) then
    begin
      bs := SC.CreateWithSession(s);
      st.Add(SC, bs);
    end else
      bs := st[SC];

   {$If CompilerVersion > 30}
    bs.RegisterForUpdates<SC>(Context,
      procedure(Store : SC)
      begin
        Event(Context, Store);
      end
    )
    {$ELSE}
    bs.RegisterForUpdates(Context,
      procedure(Store : TBaseStore)
      begin
        Event(Context, Store);
      end
    )
    {$ENDIF}
  end;
end;

class procedure TStores.RegisterForUpdates<SC>(Sessions: TArray<string>;
  Context: TObject; Handler: TProc<SC>);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if not st.ContainsKey(SC) then
    begin
      bs := SC.CreateWithSession(s);
      st.Add(SC, bs);
    end else
      bs := st[SC];

   {$If CompilerVersion > 30}
    bs.RegisterForUpdates<SC>(Context,
      procedure(Store : SC)
      begin
        Handler(Store);
      end
    )
    {$ELSE}
    bs.RegisterForUpdates(Context,
      procedure(Store : TBaseStore)
      begin
        Handler(Store);
      end
    )
    {$ENDIF}
  end;
end;

class procedure TStores.RegisterForUpdates<SC>(Session: string; Context: TObject;
  Event: TEventMethod<SC>);
begin
  RegisterForUpdates<SC>([Session], Context, Event);
end;

class procedure TStores.RegisterForUpdates<SC>(Session: string; Context: TObject;
  Handler: TProc<SC>);
begin
  RegisterForUpdates<SC>([Session], Context, Handler);
end;

class procedure TStores.RegisterForUpdates<SC>(Context: TObject; Event: TEventMethod<SC>);
begin
  RegisterForUpdates<SC>([''], Context, Event);
end;

class function TStores.Stores(Session: string): TSessionStores;
begin
  FCS.Enter;
  try
    if not FStores.ContainsKey(Session) then
    begin
      Result := TSessionStores.Create;
      FStores.Add(Session, Result);
    end;
    Result := FStores[Session];
  finally
    FCS.Leave;
  end;
end;

class procedure TStores.UnregisterForUpdates<SC>(Session: string; Context: TObject);
begin
  UnregisterForUpdates<SC>([Session], Context);
end;

class procedure TStores.UnregisterForUpdates<SC>(Sessions: TArray<string>; Context: TObject);
var
  st : TSessionStores;
  bs : TBaseStore;
  s : string;
begin
  for s in Sessions do
  begin
    st := Stores(s);
    if st.ContainsKey(SC) then
    begin
      bs := st[SC];
      bs.UnregisterForUpdates(Context);
    end;
  end;
end;

class procedure TStores.WaitForUpdate<SC>(Session: string;  LastUpdateCount : UInt64; OnData: TDataStoreHandler<SC>; Timeout : Cardinal = TBaseStore.INFINITE);
var
  st : TSessionStores;
  bs : TBaseStore;
begin
  st := Stores(Session);
  if not st.ContainsKey(SC) then
  begin
    bs := SC.CreateWithSession(Session);
    st.Add(SC, bs);
  end else
    bs := st[SC];
  if bs.UpdateCount = LastUpdateCount then
  begin
   {$If CompilerVersion > 30}
    bs.WaitForUpdate<SC>(
      procedure(Store : SC)
      begin
        OnData(Store);
      end,
      Timeout
    )
    {$ELSE}
    bs.WaitForUpdate(
      procedure(Store : TBaseStore)
      begin
        OnData(Store);
      end,
      Timeout
    )
    {$ENDIF}
  end else
    OnData(bs as SC);
end;

class procedure TStores.WithData<SC>(Session: string; OnData: TDataStoreHandler<SC>);
var
  st : TSessionStores;
  bs : TBaseStore;
begin
  st := Stores(Session);
  if not st.ContainsKey(SC) then
  begin
    bs := SC.CreateWithSession(Session);
    st.Add(SC, bs);
  end;
  bs := st[SC];
  OnData(bs as SC);
end;

class procedure TStores.WithData<SC>(OnData: TDataStoreHandler<SC>);
begin
  WithData<SC>('', OnData);
end;

class procedure TStores.UnbindAll<SC>(Target: TComponent);
begin
  UnbindAll<SC>(Target, '');
end;

class procedure TStores.UnbindAll<SC>(Target: TComponent;
  const Session: string);
begin
  WithData<SC>(Session,
    procedure(Store : SC)
    begin
      Store.UnbindAll(Target);
    end
  );
end;

class procedure TStores.UnregisterForUpdates<SC>(Context: TObject);
begin
  UnregisterForUpdates<SC>([''], Context);
end;

{ TBaseDataModuleStore }

constructor TBaseDataModuleStore.Create(AOwner: TComponent);
begin
  inherited;
  FBaseStore := TBaseStore.Create;
end;

destructor TBaseDataModuleStore.Destroy;
begin
  FBaseStore.Free;
  inherited;
end;

procedure TBaseDataModuleStore.RegisterForUpdates(Context : TObject; Handler: TProc<TBaseDataModuleStore>);
begin
  {$IF CompilerVersion > 30}
  FBaseStore.RegisterForUpdates<TBaseStore>(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end
  );
  {$ELSE}
  FBaseStore.RegisterForUpdates(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end
  );
  {$ENDIF}
end;

{$IF CompilerVersion > 30}
procedure TBaseDataModuleStore.RegisterForUpdates<SC>(Context : TObject; Handler: TProc<SC>);
begin
  FBaseStore.RegisterForUpdates<TBaseStore>(Context,
    procedure(Store : TBaseStore)
    begin
      Handler(Self);
    end
  );
end;
{$ENDIF}

procedure TBaseDataModuleStore.UnregisterForUpdates(Context: TObject);
begin
  //FBaseStore.UnregisterForUpdates(Context);
end;

procedure TBaseDataModuleStore.UpdateViews;
begin
  FBaseStore.UpdateViews;
end;

{ TBinding }

function TBinding.AsValue: TValue;
begin
  Result := FSourceProp.GetValue(FOwner);
end;

function TBinding.Target: TComponent;
begin
  Result := FTarget;
end;

constructor TBinding.Create(Owner: TBaseStore; Target: TComponent;
  const SourceProperty, TargetProperty: string);
var
  cxt : TRttiContext;
begin
  cxt := TRttiContext.Create;
  FOwner := Owner;
  FTarget := Target;
  FSourceProperty := SourceProperty;
  FTargetProperty := TargetProperty;

  FSourceProp := cxt.GetType(Owner.ClassType).GetProperty(SourceProperty);
  FTargetProp := cxt.GetType(Target.ClassType).GetProperty(TargetProperty);

  if not FTargetProp.IsWritable then
    raise EFluxBindException.Create('Target property "'+TargetProperty+'" is not writable');

  if FSourceProp.IsWritable then
    raise EFluxBindException.Create('Source property "'+SourceProperty+'" must be read only');

  if FTargetProp.PropertyType.TypeKind <> FSourceProp.PropertyType.TypeKind then
    raise EFluxBindException.Create('Types do not match for properties "'+SourceProperty+'" and "'+TargetProperty+'"');

  FIsInstanceProp := FTargetProp.PropertyType.IsInstance;

  if FIsInstanceProp then
  begin
    FSourcePropInstance := FSourceProp.GetValue(FOwner).AsObject;
    FTargetPropInstance := FTargetProp.GetValue(FTarget).AsObject;
  end else
  begin
    FSourcePropInstance := nil;
    FTargetPropInstance := nil;
  end;

end;

function TBinding.SourceProperty: string;
begin
  Result := FSourceProperty;
end;

function TBinding.TargetProperty: string;
begin
  Result := FTargetProperty;
end;

procedure TBinding.UpdateValue;
begin
  if FIsInstanceProp and
    (FSourcePropInstance is TPersistent) and
    (FTargetPropInstance is TPersistent) then
  begin
    TPersistent(FTargetPropInstance).Assign(TPersistent(FSourcePropInstance));
  end else
    FTargetProp.SetValue(FTarget, AsValue);
end;

end.
