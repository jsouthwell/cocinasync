unit cocinasync.promise.impl;

interface

uses
  System.SysUtils,
  System.SyncObjs,
  System.Generics.Collections,
  System.Generics.Defaults,
  System.Classes,
  Cocinasync.promise,
  Cocinasync.collections,
  Cocinasync.jobs;

{$IF CompilerVersion >= 340}
  {$DEFINE RESULT_SUPPORT}
{$IFEND}

type
  TProc = System.SysUtils.TProc;
  TPromiseImpl = class(TInterfacedObject, IPromise)
  strict private
    type
      TPromiseThread = class(TThread)
      private
        FCount : integer;
        FCurrentJobs : TQueue<TProc>;
        FDone : Boolean;
        FNextJobs : TQueue<TProc>;
        FQueues : TQueue<TQueue<TProc>>;
        FPromise : IPromise;
        FOnError: TAbortableExceptionHandler;
        FOnThreadFinished : TMethod;
      protected
        procedure TerminateThread;
        procedure Execute; override;
      public
        procedure AddJob(const Proc : TProc; Wait : boolean);
        property OnError : TAbortableExceptionHandler read FOnError write FOnError;
        constructor Create(Promise : IPromise; OnThreadFinished : TMethod);
        destructor Destroy; override;
      end;
  protected
    FThread : TPromiseThread;
    function AllDone : boolean; virtual;
    procedure OnThreadFinished; virtual;
  public
    function All(const ary : TArray<TProc>) : IPromise; overload;
    function All(const ary : TArray<TMethod>) : IPromise; overload;
    function All(const proc : TProc) : IPromise; overload;
    function All(const method : TMethod) : IPromise; overload;
    function &Do(Proc : TProc) : IPromise; overload;
    function &Do(Proc : TMethod) : IPromise; overload;
    function &And(Proc : TProc) : IPromise; overload;
    function &And(Proc : TMethod) : IPromise; overload;
    function &Then(Proc : TProc) : IPromise; overload;
    function &Then(Proc : TMethod) : IPromise; overload;
    function OnError(Proc : TAbortableExceptionHandler) : IPromise; overload;
    function OnError(Proc : TExceptionHandler) : IPromise; overload;
    function Go : IPromise; overload;

    constructor Create; virtual;
    destructor Destroy; override;
  end;

  {$IFDEF RESULT_SUPPORT}
  TPromiseImpl<T> = class(TPromiseImpl, IPromise<T>)
  private
    FEvent : TEvent;
    FResultValue : T;
    FOnCalculate : TFunc<T, T, T>;
    procedure SetResultValue(const Value: T);
  protected
    property ResultValue : T read FResultValue write SetResultValue;
    procedure OnThreadFinished; override;
  public
    function All(const ary : TArray<TProc>) : IPromise<T>; overload;
    function All(const ary : TArray<TMethod>) : IPromise<T>; overload;
    function All(const ary : TProc) : IPromise<T>; overload;
    function All(const ary : TMethod) : IPromise<T>; overload;
    function &Do(Proc : TProc) : IPromise<T>; overload;
    function &Do(Proc : TMethod) : IPromise<T>; overload;
    function &Do(Proc : TVarProc<T>) : IPromise<T>; overload;
    function &Do(Proc : TVarMethod<T>) : IPromise<T>; overload;
    function &And(Proc : TProc) : IPromise<T>; overload;
    function &And(Proc : TMethod) : IPromise<T>; overload;
    function &And(Proc : TVarProc<T>) : IPromise<T>; overload;
    function &And(Proc : TVarMethod<T>) : IPromise<T>; overload;
    function &Then(Proc : TProc) : IPromise<T>; overload;
    function &Then(Proc : TMethod) : IPromise<T>; overload;
    function &Then(Proc : TVarProc<T>) : IPromise<T>; overload;
    function &Then(Proc : TVarMethod<T>) : IPromise<T>; overload;
    function OnError(Proc : TAbortableExceptionHandler) : IPromise<T>; overload;
    function OnError(Proc : TExceptionHandler) : IPromise<T>; overload;
    function OnCalculate(Func : TFunc<T, T, T>) : IPromise<T>;
    function Go : IPromise<T>;
    function Result : T;

    constructor Create; override;
    destructor Destroy; override;
  end;
  {$ENDIF}

implementation

{ TPromiseImpl }

constructor TPromiseImpl.Create;
begin
  inherited Create;
  FThread := TPromiseThread.Create(Self, OnThreadFinished);
end;

destructor TPromiseImpl.Destroy;
begin
  FThread.Terminate;
  inherited;
end;

function TPromiseImpl.Go : IPromise;
begin
  FThread.FDone := True;
  Result := Self;
end;

function TPromiseImpl.OnError(Proc : TExceptionHandler): IPromise;
begin
  Result := Self;
  FThread.OnError :=
    procedure(E : Exception; var Abort : boolean)
    begin
      Proc(E);
      Abort := True;
    end;
end;

procedure TPromiseImpl.OnThreadFinished;
begin
  //
end;

function TPromiseImpl.&Do(Proc: TMethod): IPromise;
begin
  Result := &Do(
    procedure
    begin
       Proc();
    end
  );
end;

function TPromiseImpl.OnError(Proc : TAbortableExceptionHandler): IPromise;
begin
  Result := Self;
  FThread.OnError := Proc;
end;

function TPromiseImpl.All(const ary: TArray<TProc>): IPromise;
var
  p : TProc;
begin
  for p in ary do
  begin
    FThread.AddJob(p, False);
  end;
  result := Self;
end;

function TPromiseImpl.&Do(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, False);
  Result := Self;
end;

function TPromiseImpl.All(const ary: TArray<TMethod>): IPromise;
var
  p : TMethod;
begin
  for p in ary do
  begin
    FThread.AddJob(
      procedure
      begin
        p()
      end,
      False
    );
  end;
  result := Self;
end;

function TPromiseImpl.All(const proc : TProc): IPromise;
begin
  FThread.AddJob(proc, False);
  Result := Self;
end;

function TPromiseImpl.All(const method : TMethod): IPromise;
begin
  FThread.AddJob(
    procedure
    begin
      method()
    end,
    False
  );
  Result := Self;
end;

function TPromiseImpl.AllDone: boolean;
begin
  Result := FThread.FDone and (FThread.FCurrentJobs.Count = 0) and (FThread.FQueues.Count = 0);
end;

function TPromiseImpl.&And(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, False);
  Result := Self;
end;

function TPromiseImpl.&And(Proc: TMethod): IPromise;
begin
  FThread.AddJob(
    procedure
    begin
      Proc();
    end,
    False
  );
  Result := Self;
end;

function TPromiseImpl.&Then(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, True);
  Result := Self;
end;

function TPromiseImpl.&Then(Proc: TMethod): IPromise;
begin
  FThread.AddJob(
    procedure
    begin
      Proc();
    end,
    True
  );
  Result := Self;
end;

{ TPromiseImpl.TPromiseThread }

procedure TPromiseImpl.TPromiseThread.Execute;
var
  p : TProc;
begin
  repeat
    if Terminated then
      exit
    else if not FDone then
    begin
      sleep(10);
      continue;
    end;

    if FCurrentJobs.Count > 0 then
    begin
      TInterlocked.Increment(FCount);
      p := FCurrentJobs.Dequeue();
      TJobManager.Execute(
        procedure
        var
          bAbort : boolean;
        begin
          try
            try
              p();
            except
              on e: exception do
                if Assigned(FOnError) then
                begin
                  bAbort := True;
                  FOnError(E, bAbort);
                  if bAbort then
                  begin
                    TerminateThread;
                  end;
                end;
            end;
          finally
            TInterlocked.Decrement(FCount);
          end;
        end
      );
    end else
    begin
      if (FCount = 0) and (FQueues.Count > 0) then
      begin
        TMonitor.Enter(FQueues);
        try
          FCurrentJobs.Free;
          FCurrentJobs := FQueues.Dequeue;
        finally
          TMonitor.Exit(FQueues);
        end;
      end;
      if (FCount = 0) and (FQueues.Count = 0) and (FCurrentJobs.Count = 0) then
        Terminate;
    end;

    sleep(1);
  until Terminated;

  FOnThreadFinished();
  FPromise := nil;
end;

procedure TPromiseImpl.TPromiseThread.TerminateThread;
begin
  Terminate;
end;

procedure TPromiseImpl.TPromiseThread.AddJob(const Proc: TProc; Wait : boolean);
begin
  if Wait then
  begin
    TMonitor.Enter(FQueues);
    try
      FNextJobs := TQueue<TProc>.Create(1024);
      FQueues.Enqueue(FNextJobs);
      FNextJobs.Enqueue(Proc);
    finally
      TMonitor.Exit(FQueues);
    end;
  end else
    FNextJobs.Enqueue(Proc)
end;

constructor TPromiseImpl.TPromiseThread.Create(Promise : IPromise; OnThreadFinished : TMethod);
begin
  inherited Create(False);
  FCount := 0;
  FDone := False;
  FPromise := Promise;
  FreeOnTerminate := True;
  FQueues := TQueue<TQueue<TProc>>.Create(1024);
  FCurrentJobs := TQueue<TProc>.Create(1024);
  FNextJobs := FCurrentJobs;
  FOnError := nil;
  FOnThreadFinished := OnThreadFinished;
end;

destructor TPromiseImpl.TPromiseThread.Destroy;
var
  q : TQueue<TProc>;
begin
  repeat
    q := FQueues.Dequeue;
    if Assigned(q) then
      q.Free;
  until not Assigned(q);
  FCurrentJobs.Free;
  FQueues.Free;
  inherited;
end;

{$IFDEF RESULT_SUPPORT}

{ TPromiseImpl<T> }

function TPromiseImpl<T>.&And(Proc: TVarProc<T>): IPromise<T>;
begin
  Result := &And(
    procedure
    var
      val : T;
    begin
      Proc(val);
      TMonitor.Enter(Self);
      try
        if Assigned(FOnCalculate) then
          ResultValue := FOnCalculate(ResultValue, val)
        else
          ResultValue := val;
      finally
        TMonitor.Exit(Self);
      end;
    end
  );
end;

function TPromiseImpl<T>.All(const ary: TArray<TProc>): IPromise<T>;
begin
  inherited &All(ary);
  result := Self;
end;

function TPromiseImpl<T>.All(const ary: TArray<TMethod>): IPromise<T>;
begin
  inherited &All(ary);
  result := Self;
end;

function TPromiseImpl<T>.All(const ary: TProc): IPromise<T>;
begin
  inherited &All(ary);
  result := Self;
end;

function TPromiseImpl<T>.All(const ary: TMethod): IPromise<T>;
begin
  inherited &All(ary);
  result := Self;
end;

function TPromiseImpl<T>.&And(Proc: TMethod): IPromise<T>;
begin
  inherited &And(Proc);
  Result := Self;
end;

function TPromiseImpl<T>.&And(Proc: TProc): IPromise<T>;
begin
  inherited &And(Proc);
  Result := Self;
end;

function TPromiseImpl<T>.&And(Proc: TVarMethod<T>): IPromise<T>;
begin
  Result := &And(
    procedure
    var
      val : T;
    begin
      Proc(val);
      TMonitor.Enter(Self);
      try
        if Assigned(FOnCalculate) then
          ResultValue := FOnCalculate(ResultValue, val)
        else
          ResultValue := val;
      finally
        TMonitor.Exit(Self);
      end;
    end
  );
end;

constructor TPromiseImpl<T>.Create;
begin
  inherited;
  FOnCalculate := nil;
  FEvent := TPromise.EventClass.Create;
  FResultValue := Default(T);
end;

destructor TPromiseImpl<T>.Destroy;
begin
  FEvent.Free;
  inherited;
end;

function TPromiseImpl<T>.&Do(Proc: TVarMethod<T>): IPromise<T>;
begin
  Result := &Do(
    procedure
    var
      val : T;
    begin
      Proc(val);
      TMonitor.Enter(Self);
      try
        if Assigned(FOnCalculate) then
          ResultValue := FOnCalculate(ResultValue, val)
        else
          ResultValue := val;
      finally
        TMonitor.Exit(Self);
      end;
    end
  );
end;

function TPromiseImpl<T>.&Do(Proc: TProc): IPromise<T>;
begin
  inherited &Do(Proc);
  Result := Self;
end;

function TPromiseImpl<T>.&Do(Proc: TMethod): IPromise<T>;
begin
  inherited &Do(Proc);
  Result := Self;
end;

function TPromiseImpl<T>.&Do(Proc: TVarProc<T>): IPromise<T>;
begin
  Result := &Do(
    procedure
    var
      val : T;
    begin
      Proc(val);
      TMonitor.Enter(Self);
      try
        if Assigned(FOnCalculate) then
          ResultValue := FOnCalculate(ResultValue, val)
        else
          ResultValue := val;
      finally
        TMonitor.Exit(Self);
      end;
    end
  );
end;

function TPromiseImpl<T>.Go: IPromise<T>;
begin
  FThread.FDone := True;
  Result := Self;
end;

function TPromiseImpl<T>.OnCalculate(Func: TFunc<T, T, T>): IPromise<T>;
begin
  FOnCalculate := Func;
  Result := Self;
end;

function TPromiseImpl<T>.OnError(Proc: TExceptionHandler): IPromise<T>;
begin
  Result := Self;
  FThread.OnError :=
    procedure(E : Exception; var Abort : boolean)
    begin
      Proc(E);
      Abort := True;
    end;
end;

procedure TPromiseImpl<T>.OnThreadFinished;
begin
  inherited;
  FEvent.SetEvent;
end;

function TPromiseImpl<T>.OnError(Proc: TAbortableExceptionHandler): IPromise<T>;
begin
  Result := Self;
  FThread.OnError := Proc;
end;

function TPromiseImpl<T>.Result: T;
begin
  FEvent.WaitFor;
  Result := ResultValue;
end;

procedure TPromiseImpl<T>.SetResultValue(const Value: T);
begin
  FResultValue := Value;
  if AllDone then
    FEvent.SetEvent;
end;

function TPromiseImpl<T>.&Then(Proc: TVarProc<T>): IPromise<T>;
begin
  Result := &Then(
    procedure
    var
      val : T;
    begin
      Proc(val);
      TMonitor.Enter(Self);
      try
        if Assigned(FOnCalculate) then
          ResultValue := FOnCalculate(ResultValue, val)
        else
          ResultValue := val;
      finally
        TMonitor.Exit(Self);
      end;
    end
  );
end;

function TPromiseImpl<T>.&Then(Proc: TVarMethod<T>): IPromise<T>;
begin
  Result := &Then(
    procedure
    var
      val : T;
    begin
      Proc(val);
      TMonitor.Enter(Self);
      try
        if Assigned(FOnCalculate) then
          ResultValue := FOnCalculate(ResultValue, val)
        else
          ResultValue := val;
      finally
        TMonitor.Exit(Self);
      end;
    end
  );
end;

function TPromiseImpl<T>.&Then(Proc: TMethod): IPromise<T>;
begin
  inherited &Then(Proc);
  Result := Self;
end;

function TPromiseImpl<T>.&Then(Proc: TProc): IPromise<T>;
begin
  inherited &Then(Proc);
  Result := Self;
end;
{$ENDIF}


end.
