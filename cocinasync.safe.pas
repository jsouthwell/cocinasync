unit cocinasync.safe;

interface

uses
  System.SysUtils,
  System.Classes,
  System.SyncObjs;

type
  ESafetyViolation = class(Exception) end;

  TWithFuncHandler<T, R> = reference to function(const AObj : T) : R;
  TWithHandler<T> = reference to procedure(const AObj : T);
  TCreateHandler<T> = reference to procedure(out AObj : T);
  TDestroyHandler<T> = reference to procedure(var AObj : T);

  ISafe<T> = interface
    procedure &With(Handler : TWithHandler<T>);
    function IsNil : boolean;
    procedure &Set(Value : T);
    function Get : T;
    procedure CreateInstance(Handler : TCreateHandler<T> = nil);
    procedure RecreateInstance(Handler : TCreateHandler<T> = nil);
    procedure DestroyInstance;
    procedure CreateIfNil(Handler : TCreateHandler<T> = nil);
  end;

  TSafe = class
  strict private
    type
      TSafe<T> = class(TInterfacedObject, ISafe<T>)
      strict private
        FAddr : Pointer;
        FCS : TCriticalSection;
        FValue : T;
        FIsManaged : boolean;
        FIsInstance : boolean;
        FDefault : T;
        FOnDestroy : TDestroyHandler<T>;
        FOnCreate : TCreateHandler<T>;
      public
        constructor Create(OnCreate : TCreateHandler<T>; OnDestroy : TDestroyHandler<T>); reintroduce; virtual;
        destructor Destroy; override;

        procedure &With(Handler : TWIthHandler<T>);
        procedure &Set(Value : T);
        function Get : T;
        function IsNil : boolean;
        procedure CreateInstance(Handler : TCreateHandler<T> = nil);
        procedure RecreateInstance(Handler : TCreateHandler<T> = nil);
        procedure DestroyInstance;
        procedure CreateIfNil(Handler : TCreateHandler<T> = nil);
      end;
  public
    class function Make<T>(OnCreate : TCreateHandler<T>; OnDestroy: TDestroyHandler<T>): ISafe<T>; overload;
    class function Make<T>(OnCreate : TCreateHandler<T>): ISafe<T>; overload;
    class function Make<T>(OnDestroy: TDestroyHandler<T>): ISafe<T>; overload;
    class function Make<T> : ISafe<T>; overload;

    class function Return<T, R>(const Value : ISafe<T>; Handler : TWithFuncHandler<T,R>) : R;
  end;


implementation

uses
  System.Rtti,
  System.Generics.Defaults;

{ TSafe.TSafe<T> }

procedure TSafe.TSafe<T>.&Set(Value: T);
begin
  FCS.Enter;
  try
    FValue := Value;
    FAddr := @FValue;
  finally
    FCS.Leave;
  end;
end;

constructor TSafe.TSafe<T>.Create(OnCreate: TCreateHandler<T>;
  OnDestroy: TDestroyHandler<T>);
var
  cxt : TRttiContext;
  ti : TRttiType;
begin
  inherited Create;
  FCS := TCriticalSection.Create;
  FDefault := Default(T);
  FValue := FDefault;
  FAddr := @FValue;

  cxt := TRttiContext.Create;
  ti := cxt.GetType(TypeInfo(T));
  FIsManaged := ti.IsManaged;
  FIsInstance := ti.IsInstance;

  FOnCreate := OnCreate;
  FOnDestroy := OnDestroy;
  if Assigned(FOnCreate) then
    CreateInstance;
end;

procedure TSafe.TSafe<T>.CreateIfNil(Handler : TCreateHandler<T> = nil);
begin
  FCS.Enter;
  try
    if TEqualityComparer<T>.Default.Equals(FValue, FDefault) then
    begin
      if Assigned(Handler) then
        Handler(FValue)
      else if Assigned(FOnCreate) then
        FOnCreate(FValue)
      else
        raise ESafetyViolation.Create('No creation handler available for safe object.');
      FAddr := @FValue;
    end;
  finally
    FCS.Leave;
  end;
end;

procedure TSafe.TSafe<T>.CreateInstance(Handler : TCreateHandler<T> = nil);
begin
  FCS.Enter;
  try
    if not TEqualityComparer<T>.Default.Equals(FValue, FDefault) then
      raise ESafetyViolation.Create('Creating an instance over an already created instance');
    if Assigned(Handler) then
      Handler(FValue)
    else if Assigned(FOnCreate) then
      FOnCreate(FValue)
    else
      raise ESafetyViolation.Create('No creation handler available for safe object.');
    FAddr := @FValue;
  finally
    FCS.Leave;
  end;
end;

destructor TSafe.TSafe<T>.Destroy;
begin
  if not TEqualityComparer<T>.Default.Equals(FValue, FDefault) then
    DestroyInstance;
  FCS.Free;
  inherited;
end;

procedure TSafe.TSafe<T>.DestroyInstance;
var
  cxt : TRTTIContext;
  mFree : TRttiMethod;
  v : TValue;
begin
  FCS.Enter;
  try
    if FIsInstance and TEqualityComparer<T>.Default.Equals(FValue, FDefault) then
      exit;
    if not Assigned(FOnDestroy) then
    begin
      if FIsInstance and not TEqualityComparer<T>.Default.Equals(FValue, FDefault) then
      begin
        cxt := TRTTIContext.Create;
        mFree := cxt.GetType(TypeInfo(T)).GetMethod('Free');
        if mFree <> nil then
        begin
          v := TValue.From<T>(FValue);
          mFree.Invoke(v,[]);
        end else
          FValue := FDefault;
      end else
        FValue := FDefault;
    end else
      FOnDestroy(FValue);
  finally
    FCS.Leave;
  end;
end;

function TSafe.TSafe<T>.Get: T;
begin
  FCS.Enter;
  try
    Result := FValue;
  finally
    FCS.Leave;
  end;
end;

function TSafe.TSafe<T>.IsNil: boolean;
begin
  FCS.Enter;
  try
    Result := TEqualityComparer<T>.Default.Equals(FValue, FDefault);
  finally
    FCS.Leave;
  end;
end;

procedure TSafe.TSafe<T>.RecreateInstance(Handler : TCreateHandler<T> = nil);
begin
  DestroyInstance;
  CreateInstance(Handler);
end;

procedure TSafe.TSafe<T>.&With(Handler: TWIthHandler<T>);
begin
  FCS.Enter;
  try
    if FIsInstance and TEqualityComparer<T>.Default.Equals(FValue, FDefault) then
      raise ESafetyViolation.Create('Attempt to access a nil value');
    Handler(FValue);
    if FAddr <> @FValue then
      raise ESafetyViolation.Create('Address of object changed in memory.');
  finally
    FCS.Leave;
  end;
end;

{ TSafe }

class function TSafe.Make<T>(OnCreate : TCreateHandler<T>; OnDestroy: TDestroyHandler<T>): ISafe<T>;
begin
  Result := TSafe<T>.Create(OnCreate, OnDestroy);
end;

class function TSafe.Make<T>(OnCreate: TCreateHandler<T>): ISafe<T>;
begin
  Result := Make<T>(OnCreate, nil);
end;

class function TSafe.Make<T>(OnDestroy: TDestroyHandler<T>): ISafe<T>;
begin
  Result := Make<T>(nil, OnDestroy);
end;

class function TSafe.Make<T>: ISafe<T>;
begin
  Result := Make<T>(nil, nil);
end;

class function TSafe.Return<T, R>(const Value: ISafe<T>; Handler: TWithFuncHandler<T, R>): R;
var
  RResult : R;
begin
  Value.&With(
    procedure(const Value : T)
    begin
      RResult := Handler(Value);
    end
  );
  Result := RResult;
end;

end.
