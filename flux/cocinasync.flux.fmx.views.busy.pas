unit cocinasync.flux.fmx.views.busy;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  cocinasync.flux.view.fmx,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.StdCtrls, FMX.Objects;

type
  TfrmCocinasyncBusy = class(TForm)
    AniIndicator: TAniIndicator;
    Rectangle1: TRectangle;
  private
    { Private declarations }
  public
    class procedure RegisterView;
  end;

implementation

{$R *.fmx}

uses
  cocinasync.flux.fmx.actions.ui;

var
  frm : TfrmCocinasyncBusy;

{ TfrmCocinasyncBusy }

class procedure TfrmCocinasyncBusy.RegisterView;
begin
  frm := TfrmCocinasyncBusy.Create(Application);
  TViews.On<TShowBusyAction>.Show(frm,
    procedure(Action : TShowBusyAction)
    begin
      frm.Parent := Action.DockParent;
      frm.left := Action.DockParent.Left;
      frm.Top := Action.DockParent.Top;
      frm.Width := Action.DockParent.Width;
      frm.Height := Action.DockParent.Height;
      frm.BringToFront;
    end
  );
  TViews.On<THideBusyAction>.Hide(frm,
    procedure(Action : THideBusyAction)
    begin
      frm.Parent := nil;
      frm.Width := 0;
      frm.Height := 0;
    end
  );
end;

initialization
  TfrmCocinasyncBusy.RegisterView;

end.
