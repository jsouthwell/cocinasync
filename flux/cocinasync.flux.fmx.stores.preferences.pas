unit cocinasync.flux.fmx.stores.preferences;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Messaging,
  System.Rtti,
  System.IniFiles,

  FMX.Platform,

  cocinasync.flux.fmx.actions.ui,
  cocinasync.flux.store;

type
  TPreferenceStore = class(TBaseStore)
  strict private
    FPrefs : TIniFile;
    function GetBytes(name: string): TArray<Byte>;
    function GetDate(name: String): TDateTime;
    function GetGUID(name: string): TGUID;
    function GetInteger(name: string): Int64;
    function GetNumber(name: string): Double;
    function GetString(name: string): string;
  public
    constructor Create; override;
    destructor Destroy; override;

    property Strings[name : string] : string read GetString;
    property Integers[name : string] : Int64 read GetInteger;
    property Dates[name : String] : TDateTime read GetDate;
    property GUIDs[name : string] : TGUID read GetGUID;
    property Numbers[name : string] : Double read GetNumber;
    property Bytes[name : string] : TArray<Byte> read GetBytes;
  end;

implementation

{ TPreferenceStore }

destructor TPreferenceStore.Destroy;
begin
  FPrefs.Free;
  inherited;
end;

constructor TPreferenceStore.Create;
begin
  inherited;
  FPrefs := THash<string, TValue>.Create;
end;

function TPreferenceStore.GetBytes(name: string): TArray<Bytes>;
begin

end;

function TPreferenceStore.GetDate(name: String): TDateTime;
begin

end;

function TPreferenceStore.GetGUID(name: string): TGUID;
begin

end;

function TPreferenceStore.GetInteger(name: string): Int64;
begin

end;

function TPreferenceStore.GetNumber(name: string): Double;
begin

end;

function TPreferenceStore.GetString(name: string): string;
begin

end;

end.
