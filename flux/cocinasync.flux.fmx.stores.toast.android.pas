unit cocinasync.flux.fmx.stores.toast.android;

interface

uses
  System.SysUtils,
  System.Classes,
  cocinasync.flux.fmx.stores.toast;

type
  TToaster = class(TInterfacedObject, IToaster)
  private
  public
    constructor Create; reintroduce;
    destructor Destroy; override;
    procedure MakeToast(const Msg : string);
  end;

implementation

{$IFDEF ANDROID}
uses
  Androidapi.Helpers,
  Androidapi.JNI.Widget;
{$ENDIF}

{ TToaster }

constructor TToaster.Create;
begin
  inherited Create;
end;

destructor TToaster.Destroy;
begin
  inherited;
end;

procedure TToaster.MakeToast(const Msg : string);
begin
  {$IFDEF ANDROID}
  TThread.Synchronize(nil,
    procedure
    begin
      TJToast.JavaClass.makeText(TAndroidHelper.Context, StrToJCharSequence(msg), TJToast.JavaClass.LENGTH_SHORT).show;
    end
  );
  {$ENDIF}
end;

end.
