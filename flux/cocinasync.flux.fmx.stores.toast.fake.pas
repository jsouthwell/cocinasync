unit cocinasync.flux.fmx.stores.toast.fake;

interface

uses
  System.SysUtils,
  System.Classes,
  System.SyncObjs,
  System.UITypes,

  FMX.Types,

  cocinasync.async,
  cocinasync.collections,
  cocinasync.flux.fmx.stores.toast;

type
  TToaster = class(TInterfacedObject, IToaster)
  strict private
    FMessages : TStack<string>;
    FTerminated : boolean;
    FEvent : TEvent;
    FAsync : TAsync;
    procedure TryToast(const s : string);
    procedure HideForm(Sender : TObject);
    procedure CloseForm(Sender : TObject);
    procedure AnimateFloat(const Target: TFmxObject; const APropertyName: string;
      const NewValue: Single; FinishedEvent : TNotifyEvent; Duration: Single = 0.2;
      AType: TAnimationType = TAnimationType.In;
      AInterpolation: TInterpolationType = TInterpolationType.Linear);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  public
    constructor Create; reintroduce;
    destructor Destroy; override;
    procedure MakeToast(const Msg : string);
  end;

implementation

uses
  FMX.Objects,
  FMX.Forms,
  FMX.Ani;

{ TToaster }

procedure TToaster.AnimateFloat(const Target: TFmxObject;
  const APropertyName: string; const NewValue: Single; FinishedEvent : TNotifyEvent;
  Duration: Single; AType: TAnimationType; AInterpolation: TInterpolationType);
  procedure StopPropertyAnimation(const Target: TFmxObject; const APropertyName: string);
  var
    I: Integer;
  begin
    I := Target.ChildrenCount - 1;
    while I >= 0 do
    begin
      if (Target.Children[I] is TCustomPropertyAnimation) and
         (CompareText(TCustomPropertyAnimation(Target.Children[I]).PropertyName, APropertyName) = 0) then
        TFloatAnimation(Target.Children[I]).Stop;
      if I > Target.ChildrenCount then
        I := Target.ChildrenCount;
      Dec(I);
    end;
  end;
var
  Animation: TFloatAnimation;
begin
  StopPropertyAnimation(Target, APropertyName);

  //CreateDestroyer;

  Animation := TFloatAnimation.Create(Target);
  Animation.Parent := Target;
  Animation.AnimationType := AType;
  Animation.Interpolation := AInterpolation;
  Animation.Duration := Duration;
  Animation.PropertyName := APropertyName;
  Animation.StartFromCurrent := True;
  Animation.StopValue := NewValue;
  Animation.OnFinish := FinishedEvent;
  Animation.TagObject := Target;
  //FDestroyer.RegisterAnimation(Animation);
  Animation.Start;

end;

procedure TToaster.CloseForm(Sender: TObject);
begin
  TForm(TRoundRect(TFloatAnimation(Sender).TagObject).Parent).Close;
end;

constructor TToaster.Create;
begin
  inherited Create;
  FTerminated := False;
  FEvent := TEvent.Create;
  FMessages := TStack<string>.Create;
  FAsync := TAsync.Create;

  TThread.CreateAnonymousThread(
    procedure
    var
      s : string;
    begin
      while not FTerminated do
      begin
        sleep(100);
        FEvent.WaitFor;
        if FTerminated then
          break;
        repeat
          s := FMessages.pop;
          TryToast(s);
        until s = '';
      end;
    end
  ).Start;
end;

destructor TToaster.Destroy;
begin
  FTerminated := True;
  FEvent.SetEvent;
  FAsync.Free;
  FMessages.Free;
  FEvent.Free;
  inherited;
end;

procedure TToaster.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

procedure TToaster.HideForm(Sender: TObject);
begin
  FAsync.AfterDo(3000,
    procedure
    begin
      AnimateFloat(TRoundRect(TFloatAnimation(Sender).TagObject), 'Opacity', 0, CloseForm);
    end
  );
end;

procedure TToaster.MakeToast(const Msg : string);
begin
  if not FTerminated then
  begin
    FMessages.Push(Msg);
  end;
end;

procedure TToaster.TryToast(const s : string);
begin
  if (s <> '') and (not FTerminated) then
  begin
    TAsync.SynchronizeIfInThread(
      procedure
      var
        frm : TForm;
        rr : TRoundRect;
        txt : TText;
      begin
        frm := TForm.Create(Application);
        try
          frm.BeginUpdate;
          try
            frm.OnClose := FormClose;
            frm.Transparency := True;
            frm.FullScreen := True;
            frm.FormStyle := TFormStyle.StayOnTop;

            rr := TRoundRect.Create(frm);
            rr.Parent := frm;
            rr.Position.X := Screen.Width / 6;
            rr.Position.X := rr.Position.X * 3;
            rr.Width := rr.Position.X;
            rr.Position.Y := Screen.Height - (Screen.Height / 3);

            txt := TText.Create(frm);
            txt.parent := rr;
            txt.Align := TAlignLayout.Client;
            txt.Text := s;

            rr.Height := txt.TextSettings.Font.Size*3;
            rr.Opacity := 0;
          finally
            frm.EndUpdate;
          end;
          frm.Show;
          AnimateFloat(rr, 'Opacity', 1, HideForm);
        except
          on e: exception do
            frm.Free;
        end;
      end
    );
  end;
end;

end.

