unit cocinasync.promise;

interface

uses
  System.SysUtils,
  System.Classes,
  System.SyncObjs;

{$IF CompilerVersion >= 340}
  {$DEFINE RESULT_SUPPORT}
{$IFEND}

type
  TEventClass = class of TEvent;

  TAbortableExceptionHandler = reference to procedure(E : Exception; var Abort : boolean);
  TExceptionHandler = reference to procedure(E : Exception);

  TVarProc<T> = reference to procedure(out Value : T);

  TMethod = procedure of object;
  TVarMethod<T> = procedure(out Value : T) of object;

  IPromise = interface
    function All(const ary : TArray<TProc>) : IPromise; overload;
    function All(const ary : TArray<TMethod>) : IPromise; overload;
    function All(const ary : TProc) : IPromise; overload;
    function All(const ary : TMethod) : IPromise; overload;
    function &Do(Proc : TProc) : IPromise; overload;
    function &Do(Proc : TMethod) : IPromise; overload;
    function &And(Proc : TProc) : IPromise; overload;
    function &And(Proc : TMethod) : IPromise; overload;
    function &Then(Proc : TProc) : IPromise; overload;
    function &Then(Proc : TMethod) : IPromise; overload;
    function OnError(Proc : TAbortableExceptionHandler) : IPromise; overload;
    function OnError(Proc : TExceptionHandler) : IPromise; overload;
    function Go : IPromise;
  end;

  {$IFDEF RESULT_SUPPORT}

  IPromise<T> = interface
    function All(const ary : TArray<TProc>) : IPromise<T>; overload;
    function All(const ary : TArray<TMethod>) : IPromise<T>; overload;
    function All(const ary : TProc) : IPromise<T>; overload;
    function All(const ary : TMethod) : IPromise<T>; overload;
    function &Do(Proc : TProc) : IPromise<T>; overload;
    function &Do(Proc : TMethod) : IPromise<T>; overload;
    function &Do(Proc : TVarProc<T>) : IPromise<T>; overload;
    function &Do(Proc : TVarMethod<T>) : IPromise<T>; overload;
    function &And(Proc : TProc) : IPromise<T>; overload;
    function &And(Proc : TMethod) : IPromise<T>; overload;
    function &And(Proc : TVarProc<T>) : IPromise<T>; overload;
    function &And(Proc : TVarMethod<T>) : IPromise<T>; overload;
    function &Then(Proc : TProc) : IPromise<T>; overload;
    function &Then(Proc : TMethod) : IPromise<T>; overload;
    function &Then(Proc : TVarProc<T>) : IPromise<T>; overload;
    function &Then(Proc : TVarMethod<T>) : IPromise<T>; overload;
    function OnError(Proc : TAbortableExceptionHandler) : IPromise<T>; overload;
    function OnError(Proc : TExceptionHandler) : IPromise<T>; overload;
    function OnCalculate(Func : TFunc<T, T, T>) : IPromise<T>;
    function Go : IPromise<T>;
    function Result : T;
  end;

  {$ENDIF}
  TPromise = class
  private
    class var FEventClass: TEventClass;
  public
    class function Prepare(Proc : TProc = nil) : IPromise; overload;
    {$IFDEF RESULT_SUPPORT}
    class function Prepare<T>(Proc : TProc) : IPromise<T>; overload;
    class function Prepare<T>(Proc : TVarProc<T> = nil) : IPromise<T>; overload;
    {$ENDIF}
    class property EventClass : TEventClass read FEventClass write FEventClass;

    class constructor Create;
  end;

  function Promise : IPromise;

implementation

uses
  cocinasync.promise.impl;

function Promise : IPromise;
begin
  Result := TPromise.Prepare(nil);
end;

{ TPromise}

class constructor TPromise.Create;
begin
  FEventClass := TEvent;
end;

class function TPromise.Prepare(Proc : TProc = nil) : IPromise;
begin
  Result := TPromiseImpl.Create;
  if Assigned(Proc) then
    Result.&Do(Proc);
end;

{$IFDEF RESULT_SUPPORT}
class function TPromise.Prepare<T>(Proc : TProc) : IPromise<T>;
begin
  Result := TPromiseImpl<T>.Create;
  if Assigned(Proc) then
    Result.&Do(Proc);
end;

class function TPromise.Prepare<T>(Proc : TVarProc<T> = nil) : IPromise<T>;
begin
  Result := TPromiseImpl<T>.Create;
  if Assigned(Proc) then
    Result.&Do(Proc);
end;

{$ENDIF}

end.

