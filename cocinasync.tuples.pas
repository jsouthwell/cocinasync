unit cocinasync.tuples;

interface

uses
  System.SysUtils,
  System.Classes;

type
  TTuple<T0, T1> = record
    v0 : T0;
    v1 : T1;
    constructor Create(const Val0 : T0; const Val1 : T1);
  end;

  TTuple<T0, T1, T2> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2);
  end;

  TTuple<T0, T1, T2, T3> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    v3 : T3;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2; const Val3 : T3);
  end;

  TTuple<T0, T1, T2, T3, T4> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    v3 : T3;
    v4 : T4;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2; const Val3 : T3; const Val4 : T4);
  end;

  TTuple<T0, T1, T2, T3, T4, T5> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    v3 : T3;
    v4 : T4;
    v5 : T5;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2; const Val3 : T3; const Val4 : T4; const Val5 : T5);
  end;

  TTuple<T0, T1, T2, T3, T4, T5, T6> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    v3 : T3;
    v4 : T4;
    v5 : T5;
    v6 : T6;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2; const Val3 : T3; const Val4 : T4; const Val5 : T5; const Val6 : T6);
  end;

  TTuple<T0, T1, T2, T3, T4, T5, T6, T7> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    v3 : T3;
    v4 : T4;
    v5 : T5;
    v6 : T6;
    v7 : T7;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2; const Val3 : T3; const Val4 : T4; const Val5 : T5; const Val6 : T6; const Val7 : T7);
  end;

  TTuple<T0, T1, T2, T3, T4, T5, T6, T7, T8> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    v3 : T3;
    v4 : T4;
    v5 : T5;
    v6 : T6;
    v7 : T7;
    v8 : T8;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2; const Val3 : T3; const Val4 : T4; const Val5 : T5; const Val6 : T6; const Val7 : T7; const Val8 : T8);
  end;

  TTuple<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> = record
    v0 : T0;
    v1 : T1;
    v2 : T2;
    v3 : T3;
    v4 : T4;
    v5 : T5;
    v6 : T6;
    v7 : T7;
    v8 : T8;
    v9 : T9;
    constructor Create(const Val0 : T0; const Val1 : T1; const Val2 : T2; const Val3 : T3; const Val4 : T4; const Val5 : T5; const Val6 : T6; const Val7 : T7; const Val8 : T8; const Val9 : T9);
  end;

implementation

{ TTuple<T0, T1> }

constructor TTuple<T0, T1>.Create(const Val0: T0; const Val1: T1);
begin
  v0 := Val0;
  v1 := Val1;
end;

{ TTuple<T0, T1, T2> }

constructor TTuple<T0, T1, T2>.Create(const Val0: T0; const Val1: T1;
  const Val2: T2);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
end;

{ TTuple<T0, T1, T2, T3> }

constructor TTuple<T0, T1, T2, T3>.Create(const Val0: T0; const Val1: T1;
  const Val2: T2; const Val3: T3);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
  v3 := Val3;
end;

{ TTuple<T0, T1, T2, T3, T4> }

constructor TTuple<T0, T1, T2, T3, T4>.Create(const Val0: T0; const Val1: T1;
  const Val2: T2; const Val3: T3; const Val4: T4);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
  v3 := Val3;
  v4 := Val4;
end;

{ TTuple<T0, T1, T2, T3, T4, T5> }

constructor TTuple<T0, T1, T2, T3, T4, T5>.Create(const Val0: T0;
  const Val1: T1; const Val2: T2; const Val3: T3; const Val4: T4; const Val5 :
  T5);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
  v3 := Val3;
  v4 := Val4;
  v5 := Val5;
end;

{ TTuple<T0, T1, T2, T3, T4, T5, T6> }

constructor TTuple<T0, T1, T2, T3, T4, T5, T6>.Create(const Val0: T0;
  const Val1: T1; const Val2: T2; const Val3: T3; const Val4: T4; const Val5 :
  T5; const Val6: T6);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
  v3 := Val3;
  v4 := Val4;
  v5 := Val5;
  v6 := Val6;
end;

{ TTuple<T0, T1, T2, T3, T4, T5, T7> }

constructor TTuple<T0, T1, T2, T3, T4, T5, T6, T7>.Create(const Val0 : T0;
  const Val1 : T1; const Val2 : T2; const Val3 : T3; const Val4 : T4;
  const Val5 : T5; const Val6 : T6; const Val7 : T7);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
  v3 := Val3;
  v4 := Val4;
  v5 := Val5;
  v6 := Val6;
  v7 := Val7;
end;

{ TTuple<T0, T1, T2, T3, T4, T5, T7, T8> }

constructor TTuple<T0, T1, T2, T3, T4, T5, T6, T7, T8>.Create(const Val0: T0;
  const Val1: T1; const Val2: T2; const Val3: T3; const Val4: T4; const Val5 :
  T5; const Val6: T6; const Val7: T7; const Val8: T8);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
  v3 := Val3;
  v4 := Val4;
  v5 := Val5;
  v6 := Val6;
  v7 := Val7;
  v8 := Val8;
end;

{ TTuple<T0, T1, T2, T3, T4, T5, T7, T8, T9> }

constructor TTuple<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>.Create(const Val0: T0;
  const Val1: T1; const Val2: T2; const Val3: T3; const Val4: T4; const Val5 :
  T5; const Val6: T6; const Val7: T7; const Val8: T8; const Val9: T9);
begin
  v0 := Val0;
  v1 := Val1;
  v2 := Val2;
  v3 := Val3;
  v4 := Val4;
  v5 := Val5;
  v6 := Val6;
  v7 := Val7;
  v8 := Val8;
  v9 := Val9;
end;

end.
