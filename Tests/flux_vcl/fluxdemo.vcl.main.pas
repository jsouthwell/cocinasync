unit fluxdemo.vcl.main;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.Menus,
  Vcl.CheckLst;

type
  TfrmToDoList = class(TForm)
    txtItem: TEdit;
    btnAdd: TButton;
    pmItem: TPopupMenu;
    miDelete: TMenuItem;
    miCompleted: TMenuItem;
    lbItems: TCheckListBox;
    N2: TMenuItem;
    Generateinthreads1: TMenuItem;
    N1: TMenuItem;
    miHistory: TMenuItem;
    procedure txtItemChange(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbItemsClickCheck(Sender: TObject);
    procedure miDeleteClick(Sender: TObject);
    procedure Generateinthreads1Click(Sender: TObject);
    procedure pmItemPopup(Sender: TObject);
    procedure miHistoryClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmToDoList: TfrmToDoList;

implementation

uses
  fluxdemo.actions,
  cocinasync.flux,
  fluxdemo.Stores.Item,
  cocinasync.jobs,
  System.SyncObjs;

{$R *.dfm}

procedure TfrmToDoList.btnAddClick(Sender: TObject);
begin
  TNewItemAction.Send(dmItemStore.NewID, txtItem.Text);
end;

procedure TfrmToDoList.FormCreate(Sender: TObject);
var
  bInlineCall : boolean;
begin
  bInlineCall := True;
  dmItemStore.RegisterForUpdates<TdmItemStore>(Self,
    procedure(Store : TdmItemStore)
    var
      i : integer;
    begin
      lbItems.Items.BeginUpdate;
      try
        lbItems.Clear;
        for i := 0 to Store.Count-1 do
        begin
          lbItems.Items.Add(Store.Items[i].Text);
          lbItems.Checked[i] := Store.Items[i].Checked;
        end;
      finally
        lbItems.Items.EndUpdate;
      end;
      if not bInlineCall then  // calling SetFocus in FormCreate causes "Cannot focus a disabled window"
      begin
        txtItem.SelectAll;
        txtItem.SetFocus;
      end;
    end
  );
  bInlineCall := False;
end;

procedure TfrmToDoList.FormDestroy(Sender: TObject);
begin
  dmItemStore.UnregisterForUpdates(Self);
end;

procedure TfrmToDoList.Generateinthreads1Click(Sender: TObject);
var
  i: Integer;
  cnt : integer;
begin
  cnt := 0;
  for i := 0 to 100 do
  begin
    TJobManager.Execute(
      procedure
      var
        iCnt : integer;
      begin
        iCnt := TInterlocked.Increment(cnt);
        TNewItemAction.Send(dmItemStore.NewID, 'Thread Item '+IntToStr(iCnt));
      end
    );
  end;
end;

procedure TfrmToDoList.lbItemsClickCheck(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbItems.Count-1 do
    if lbItems.Checked[i] <> dmItemStore.Items[i].Checked then
    begin
      TToggleItemAction.Send(dmItemStore.Items[i].ID, lbItems.Checked[i] );
    end;
end;

procedure TfrmToDoList.miDeleteClick(Sender: TObject);
begin
  if lbItems.ItemIndex >= 0 then
    TDeleteItemAction.Send(dmItemStore.Items[lbItems.ItemIndex].ID);
end;

procedure TfrmToDoList.miHistoryClick(Sender: TObject);
begin
  TToggleHistoryAction.Send;
end;

procedure TfrmToDoList.pmItemPopup(Sender: TObject);
begin
  miDelete.Enabled := lbItems.ItemIndex >= 0;
end;

procedure TfrmToDoList.txtItemChange(Sender: TObject);
begin
  btnAdd.Enabled := txtItem.Text <> '';
end;

end.
