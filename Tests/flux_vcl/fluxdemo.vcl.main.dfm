object frmToDoList: TfrmToDoList
  Left = 0
  Top = 0
  Caption = 'To Do List'
  ClientHeight = 157
  ClientWidth = 279
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    279
    157)
  PixelsPerInch = 96
  TextHeight = 13
  object txtItem: TEdit
    Left = 16
    Top = 16
    Width = 177
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = txtItemChange
  end
  object btnAdd: TButton
    Left = 199
    Top = 16
    Width = 66
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Add'
    Default = True
    Enabled = False
    TabOrder = 1
    OnClick = btnAddClick
  end
  object lbItems: TCheckListBox
    Left = 16
    Top = 43
    Width = 249
    Height = 97
    OnClickCheck = lbItemsClickCheck
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    PopupMenu = pmItem
    TabOrder = 2
  end
  object pmItem: TPopupMenu
    OnPopup = pmItemPopup
    Left = 136
    Top = 88
    object miCompleted: TMenuItem
      Caption = 'Completed'
    end
    object miDelete: TMenuItem
      Caption = 'Delete'
      OnClick = miDeleteClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Generateinthreads1: TMenuItem
      Caption = 'Generate in threads'
      OnClick = Generateinthreads1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object miHistory: TMenuItem
      Caption = 'Toggle History'
      OnClick = miHistoryClick
    end
  end
end
