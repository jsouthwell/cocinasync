unit promises.test;

interface

procedure TestPromise1;
procedure TestPromise2;

var
  Terminated : boolean;

implementation

uses
  System.SysUtils,
  System.Classes,
  cocinasync.promise,
  cocinasync.async,
  cocinasync.event.console
  ;

procedure TestPromise1;
var
  sl : TStringList;
begin
  sl := TStringList.Create;
  sl.Add('start');
  TPromise.Prepare(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 20;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('A1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&And(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('A2 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
      //raise Exception.Create('Hey you');
    end
  ).&Then(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 10;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).All([
    procedure
    var
      iVal : Integer;
    begin
      iVal := 20;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B2a : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end,
    procedure
    var
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B2b : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ]).&Then(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 100;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('C1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&And(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 200;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('C2 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&Then(
    procedure
    var
      s : string;
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('D1 : '+iVal.ToString);
        s := sl.Text;
      finally
        TMonitor.Exit(sl);
      end;
      TAsync.SynchronizeIfInThread(
        procedure
        begin
          WriteLn(s);
        end
      );
      sl.Free;
      Terminated := True;
    end
  ).OnError(
    procedure(E : Exception; var Abort : boolean)
    begin
      TMonitor.Enter(sl);
      try
        sl.Add('Exception encountered: '+E.Message);
      finally
        TMonitor.Exit(sl);
      end;
      Abort := False;
    end
  ).Go;
  TMonitor.Enter(sl);
  try
    sl.Add('exiting');
  finally
    TMonitor.Exit(sl);
  end;

end;

procedure TestPromise2;
var
  sl : TStringList;
  p : IPromise<Integer>;
begin
  sl := TStringList.Create;
  sl.Add('start');
  p := TPromise.Prepare<Integer>(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 20;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('A1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&And(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('A2 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
      //raise Exception.Create('Hey you');
    end
  ).&Then(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 10;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).All([
    procedure
    var
      iVal : Integer;
    begin
      iVal := 20;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B2a : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end,
    procedure
    var
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B2b : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ]).&Then(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 100;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('C1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&And(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 200;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('C2 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&Then(
    procedure
    var
      s : string;
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('D1 : '+iVal.ToString);
        s := sl.Text;
      finally
        TMonitor.Exit(sl);
      end;
      TAsync.SynchronizeIfInThread(
        procedure
        begin
          WriteLn(s);
        end
      );
      sl.Free;
      Terminated := True;
    end
  ).&Then(
    procedure(out Value : Integer)
    begin
      value := 1010;
    end
  ).OnError(
    procedure(E : Exception; var Abort : boolean)
    begin
      TMonitor.Enter(sl);
      try
        sl.Add('Exception encountered: '+E.Message);
      finally
        TMonitor.Exit(sl);
      end;
      Abort := False;
    end
  ).Go;
  if p.result <> 1010 then
    raise Exception.Create('result not set');
  TMonitor.Enter(sl);
  try
    sl.Add('exiting');
  finally
    TMonitor.Exit(sl);
  end;

end;

initialization
  TNonBlockingEvent.UnblockPromises;
  Terminated := False;
  Randomize;

end.
