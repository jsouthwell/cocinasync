unit fluxwb.stores.item;

interface

uses
  System.SysUtils,
  System.Classes,
  cocinasync.flux.store,
  System.Generics.Collections;

type
  TItemStore = class(TBaseStore)
  type
    TToDoItem = class(TObject)
      ID : TGuid;
      Text : String;
      Checked : boolean;
      constructor Create(ID : TGUID; Text : String; Checked : Boolean);
    end;
  private
    FToDoItems : TList<TToDoItem>;
    function GetCount: integer;
    function GetItem(idx: integer): TTodoItem;
  public
    property Count : integer read GetCount;
    property Items[idx : integer] : TTodoItem read GetItem;
    function NewID : TGUID;

    constructor Create; override;
    destructor Destroy; override;
  end;

implementation

uses
  cocinasync.flux,
  fluxwb.Actions;

constructor TItemStore.Create;
begin
  inherited;
  FToDoItems := TList<TToDoItem>.Create;

  Flux.Register<TNewItemAction>(Session, Self,
    procedure(Action : TNewItemAction)
    begin
      FToDoItems.Insert(0, TToDoItem.Create(Action.ID, Action.Text, False));
    end
  );

  Flux.Register<TDeleteItemAction>(Session, Self,
    procedure(Action : TDeleteItemAction)
    var
      item : TToDoItem;
    begin
      for item in FToDoItems do
      begin
        if item.ID = Action.ID then
        begin
          FToDoItems.Extract(item);
          item.Free;
          break;
        end;
      end;
    end
  );

  Flux.Register<TToggleItemAction>(Session, Self,
    procedure(Action : TToggleItemAction)
    var
      item : TToDoItem;
    begin
      for item in FToDoItems do
      begin
        if item.ID = Action.ID then
        begin
          item.Checked := Action.Checked;
          break;
        end;
      end;
    end
  );
end;

destructor TItemStore.Destroy;
var
  item: TToDoItem;
begin
  for item in FToDoItems do
  begin
    item.Free;
  end;
  FToDoItems.Free;
end;

function TItemStore.GetCount: integer;
begin
  Result := FToDoItems.Count;
end;

function TItemStore.GetItem(idx: integer): TTodoItem;
begin
  Result := FToDoItems[idx];
end;

function TItemStore.NewID: TGUID;
begin
  CreateGUid(Result);
end;

{ TItemStore.TToDoItem }

constructor TItemStore.TToDoItem.Create(ID: TGUID; Text: String;
  Checked: Boolean);
begin
  inherited Create;
  Self.ID := ID;
  Self.Text := Text;
  Self.Checked := Checked;
end;

end.
