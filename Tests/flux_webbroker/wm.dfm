object Web: TWeb
  OldCreateOrder = False
  Actions = <
    item
      Default = True
      Name = 'DefaultHandler'
      PathInfo = '/'
      OnAction = WebDefaultHandlerAction
    end
    item
      MethodType = mtPost
      Name = 'actAddTodo'
      PathInfo = '/todo_add'
      OnAction = WebactAddTodoAction
    end
    item
      MethodType = mtPost
      Name = 'actListTodo'
      PathInfo = '/todo_list'
      OnAction = WebactListTodoAction
    end
    item
      MethodType = mtPost
      Name = 'actLog'
      PathInfo = '/log'
      OnAction = WebactLogAction
    end
    item
      MethodType = mtPost
      Name = 'actToggleTodo'
      PathInfo = '/todo_toggle'
      OnAction = WebactToggleTodoAction
    end>
  BeforeDispatch = WebModuleBeforeDispatch
  AfterDispatch = WebModuleAfterDispatch
  Height = 230
  Width = 415
end
